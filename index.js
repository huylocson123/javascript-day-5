document.getElementById("btn-tuyen-sinh").addEventListener("click", function() {
    var selectKhuVuc = document.getElementById("diem-khu-vuc");
    var diemKhuVuc = selectKhuVuc.options[selectKhuVuc.selectedIndex].value * 1;

    var selectDoituong = document.getElementById("diem-doi-tuong");
    var diemDoituong = selectDoituong.options[selectDoituong.selectedIndex].value * 1;

    var mark1 = document.getElementById("mark1").value * 1;
    var mark2 = document.getElementById("mark2").value * 1;
    var mark3 = document.getElementById("mark3").value * 1;
    var sum = mark1 + mark2 + mark3 + diemKhuVuc + diemDoituong;
    var diemChuan = document.getElementById("diem-chuan").value * 1;

    if (mark1 == 0 || mark2 == 0 || mark3 == 0) {
        document.getElementById("kq-tuyen-sinh").value = "Bạn đã rớt vì có môn điểm 0 ";
    } else if (sum >= diemChuan) {
        document.getElementById("kq-tuyen-sinh").value = `Bạn đã trúng tuyển, điểm tổng của bạn : ` + sum;
    } else if (sum < diemChuan) {
        document.getElementById("kq-tuyen-sinh").value = `Bạn đã rớt vì điểm tổng kết bé hơn điểm chuẩn, điểm tổng của bạn : ` + sum;
    }

});


// Bài 2



document.getElementById("btn-tien-dien").addEventListener("click", function() {

    var soKw = document.getElementById("kw").value * 1;
    var tien = 0;

    if (soKw <= 50) {
        tien = soKw * 500;
    } else if (soKw <= 100) {
        tien = 50 * 500 + (soKw - 50) * 650;

    } else if (soKw <= 200) {
        tien = 50 * 500 + 50 * 650 + (soKw - 100) * 850;
    } else if (soKw <= 350) {
        tien = 50 * 500 + 50 * 650 + 100 * 850 + (soKw - 200) * 1100;
    } else {
        tien = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
    }




    var ten = document.getElementById("ten").value;


    document.getElementById("kq-tien-dien").value = "Tiền điện của " + ten + "là: " + tien;


});